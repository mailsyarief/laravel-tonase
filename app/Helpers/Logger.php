<?php

namespace App\Helpers;

use App\Models\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class Logger
{
    public static function insert($user_id,$request, $info = "")
    {
        $log = new Log();
        $log->user_id = $user_id;
        $log->ip = $request->ip();
        $log->path = URL::current();
        $log->method = $request->method();
        $log->info = $info;
        $log->save();
    }
}
