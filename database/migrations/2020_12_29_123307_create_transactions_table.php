<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('reference_id');
            $table->decimal('before_balance',15,3)->default(0);
            $table->decimal('debit',15,3)->default(0);
            $table->decimal('credit',15,3)->default(0);
            $table->decimal('after_balance',15,3)->default(0);
            $table->string('type')->comment("1 : topup, 2 : widhdraw, 3 : transfer");
            $table->string('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
