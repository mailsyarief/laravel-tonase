<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '018bb5e7-3e3e-410d-b68b-c1c75f2c1ee1',
            'name' => 'seeder',
            'email' => 'seeder@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
